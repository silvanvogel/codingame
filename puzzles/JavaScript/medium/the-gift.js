const N = parseInt(readline()); // number of participants
let C = parseInt(readline());   // cost of gift
let contributions = []
let availableBudget = 0

for (let i = 0; i < N; i++) {
    const B = parseInt(readline());
    contributions.push(B);
    availableBudget += B;
}

if(availableBudget < C) {
    console.log("IMPOSSIBLE");
} else {
    // Sort array
    contributions.sort(function(a, b) {
        return a - b;
    });

    for(let i = 0; i < N; i++) {
        remaining = Math.floor(C / (N - i));
        minContribution = Math.min(contributions[i], remaining);
        
        console.log(minContribution);
        C-=minContribution;
    }
}