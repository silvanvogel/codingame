var charLength = parseInt(readline()); //length of each letter
var rowHeight = parseInt(readline()); //height of each row
var letters = readline().toUpperCase().split('');

var alphabetArray = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ?'.split('');
var alphabetMap = {};
for (var j = alphabetArray.length - 1; j >= 0; j--) {
    alphabetMap[alphabetArray[j]] = [];
}

for (var i = 0; i < rowHeight; i++) {
    var ROW = readline();
    var currentLetterIndex = 0;
    for (var j = 0; j < alphabetArray.length; j++) {
        var thisLetter = alphabetArray[j];
        alphabetMap[thisLetter][i] = ROW.substr(currentLetterIndex, charLength);
        currentLetterIndex += charLength;
    }
}

var result = '';
for (var j = 0; j < rowHeight; j++) { //each line by height
    for (var i = 0; i < letters.length; i++) { //each letter in output
        var thisLetter = letters[i];
        if ((thisLetter < 'A') || (thisLetter > 'Z')) {
            thisLetter = '?';
        }
        result += alphabetMap[thisLetter][j];
    }
    result += '\n';
}

print(result);