const N = parseInt(readline());
var arr = [];

for (let i = 0; i < N; i++) {
    arr.push(parseInt(readline()));
}

arr = arr.sort((a,b) => a-b)
min = 100000;

for (i = 0; i < N-1; i++){
    if (min > Math.abs(arr[i] -  arr[i+1])){
        min = Math.abs(arr[i] -  arr[i+1])
    }
}
console.log(min)