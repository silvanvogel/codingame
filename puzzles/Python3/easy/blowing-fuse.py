n, m, c = [int(i) for i in input().split()]                 # devices, clicks, capacity in A
deviceStates = [0] * n                                      # device states (on/off)
deviceCapacities = list(map(int, input().split()))          # current consumption of each device
maxConsumption, currentConsumption = 0, 0

# loop through click sequence
for i in map(int, input().split()):
    i-=1
    deviceStates[i] = not deviceStates[i]
    if deviceStates[i]:
        currentConsumption += deviceCapacities[i]
    else:
        currentConsumption -= deviceCapacities[i]
    
    maxConsumption = max(maxConsumption, currentConsumption)
    
if maxConsumption > c:
    print("Fuse was blown.")
else:
    print("Fuse was not blown.")
    print(f"Maximal consumed current was {maxConsumption} A.")