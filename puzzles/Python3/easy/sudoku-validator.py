horizontal=[]
vertical=[[], [], [], [], [], [], [], [], []]

checkHorizontal = True
checkVertical = True

def checkSubgrid():
    for row in range(0,9,3):
        for col in range(0,9,3):
            temp = []
            for r in range(row,row+3):
                for c in range(col, col+3):
                    if horizontal[r][c] != 0:
                        temp.append(horizontal[r][c])
                if any(i < 0 and i > 9 for i in temp):
                    return False
                elif len(temp) != len(set(temp)):
                    return False
    return True

for i in range(9):
    c = 0
    h = []
    for j in input().split():
        n = int(j)
        h.append(n)
        vertical[c].append(n)
        c += 1
    horizontal.append(h)

for i in range(1,9):
    for j in range(1,9):
        if j not in horizontal[i]:
            checkHorizontal = False
        if j not in vertical[i]:
            checkVertical = False

if checkVertical and checkHorizontal and checkSubgrid():
    print("true")
else:
    print("false")
