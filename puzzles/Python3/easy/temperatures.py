lowest = 6000
input()
for i in input().split():
    t = int(i) # temperature

    if abs(t) < abs(lowest):
        lowest = t
    
    if(abs(lowest) == t):
        lowest = abs(t)

if lowest == 6000:
    print(0)
else:
    print(lowest)
