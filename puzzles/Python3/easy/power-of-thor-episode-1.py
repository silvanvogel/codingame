light_x, light_y, initial_tx, initial_ty = [int(i) for i in input().split()]
thor_x = initial_tx
thor_y = initial_ty
while True:
    remaining_turns = int(input())

    directionX, directionY="", ""
    if thor_x < light_x:
        directionX="E"
        thor_x-=1
    elif thor_x > light_x:
        directionX="W"
        thor_x+=1
    if thor_y < light_y:
        directionY="S"
        thor_y+=1
    elif thor_y > light_y:
        directionY="N"
        thor_y-=1
    
    print (directionY+directionX)