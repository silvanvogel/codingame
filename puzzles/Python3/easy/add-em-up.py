n, nums, cost = int(input()), sorted(map(int, input().split())), 0

while len(nums) > 1:
    newCard = nums[0] + nums[1]
    cost += newCard
    nums = sorted([newCard] + nums[2:]) # reduce integer list --> [1 + 2 + 3] = [3 + 3] = [6]

print(cost)