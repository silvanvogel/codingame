invalidISBNs, ISBN10CheckDigits = [], [10,9,8,7,6,5,4,3,2]

def calcCheckDigit(digit, modulus):
    digit = digit % modulus
    if digit != 0:
        digit = modulus - digit
        if digit == 10:
            digit = "X"
    return str(digit)

def checkISBN10(isbn):
    digits, tot = isbn[:-1], 0
    if digits.isdigit():
        for i in range(len(digits)):
            tot += int(digits[i]) * ISBN10CheckDigits[i]

        if calcCheckDigit(tot, 11) != isbn[-1]:
            invalidISBNs.append(isbn)
    else:
        invalidISBNs.append(isbn)

def checkISBN13(isbn):
    digits, tot = isbn[:-1], 0
    if digits.isdigit():
        for i in range(len(digits)):
            if i % 2 == 1: 
                tot += int(digits[i]) * 3
            else: 
                tot += int(digits[i])
                       
        if calcCheckDigit(tot, 10) != isbn[-1]:
            invalidISBNs.append(isbn)
    else:
        invalidISBNs.append(isbn)

for i in range(int(input())):
    isbn = input()
    if len(isbn) == 10:
        checkISBN10(isbn)
    elif len(isbn) == 13:
        checkISBN13(isbn)
    else:
        invalidISBNs.append(isbn)

print(f"{len(invalidISBNs)} invalid:")
print(*invalidISBNs,sep='\n')