import numpy as np

matrix = [[0 for i in range(3)] for i in range(3)]

for i in range(3):
    line = input()
    for j in range(3):
        matrix[i][j] = line[j]

def checkWinnable():
    arr = np.array(matrix)
    flatten_arr = np.ravel(arr)
    res = np.all(arr==flatten_arr[0])
    return res

def checkHorizontal():
    for i in range(3):
        counter=0
        jLocation=[]
        for j in range(3):
            if matrix[i][j] == "O" and matrix[i][j] != "X":
                jLocation.append(str(j))
                counter+=1
            else:
                jLocation.append('.')
        if counter == 2:
            index = jLocation.index('.')
            matrix[i][index] = "O"

def checkVertical():
    for i in range(3):
        counter=0
        jLocation=[]
        for j in range(3):
            if matrix[j][i] == "O" and matrix[i][j] != "X":
                jLocation.append(str(j))
                counter+=1
            else:
                jLocation.append('.')
        if counter == 2:
            index = jLocation.index('.')
            matrix[index][i] = "O"

def checkDiagonal():
    cLeft, cRight = 0, 0
    for i in range(3):
        for j in range(3):
            if i == j and matrix[i][j] == "O" and matrix[i][j] != "X":
                cLeft += 1
            elif i+j == 2 and matrix[i][j] == "O" and matrix[i][j] != "X":
                cRight += 1
    
    if cLeft == 2:
        if matrix[0][0] and matrix[1][1] and matrix[2][2] != "X":
            matrix[0][0] = "O"
            matrix[1][1] = "O"
            matrix[2][2] = "O"
    elif cRight == 2:
        if matrix[0][2] and matrix[1][1] and matrix[2][0] != "X":
            matrix[0][2] = "O"
            matrix[1][1] = "O"
            matrix[2][0] = "O"

if checkWinnable():
    print("false")
else:
    checkHorizontal()
    checkVertical()
    checkDiagonal()

    for i in matrix:
        print("".join(map(str,i)))