n = int(input())
l = []

for i in range(n): 
    l.append(int(input()))

l.sort()
m = 100000

for i in range(n-1):
    if m > abs(l[i] - l[i+1]):
        m = abs(l[i] - l[i+1])
print(m)
