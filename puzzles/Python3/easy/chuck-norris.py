message = ''.join(format(ord(x), 'b').zfill(7) for x in input())

output = ''
for index, n in enumerate(message):
    if index >= 1 and message[index] == message[index-1]:
        output+='0'
    else: 
        if n == '1':
            output+=' 0 0'
        elif n == '0':
            output+=' 00 0'
print(output[1:])