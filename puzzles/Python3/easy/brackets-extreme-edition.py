expression = input()
openedBrackets, closedBrackets = ["[","{","("], ["]","}",")"]  

def check(expr): 
    brackets = [] 
    for i in expr: 
        if i in openedBrackets:
            brackets.append(i) 
        elif i in closedBrackets: 
            pos = closedBrackets.index(i) 
            if ((len(brackets) > 0) and (openedBrackets[pos] == brackets[len(brackets)-1])): 
                brackets.pop() 
            else: 
                return "false"
    if len(brackets) == 0: 
        return "true"
    else: 
        return "false"
        
print(check(expression)) 