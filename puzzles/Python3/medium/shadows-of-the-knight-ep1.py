w, h = [int(i) for i in input().split()] # width, height of building
n = int(input())  # maximum number of turns before game over.
x, y = [int(i) for i in input().split()] # location of next window (x: horizontal, y: vertical)

x0 = 0
y0 = 0
x1 = w - 1
y1 = h - 1

# game loop
while True:
    bomb_dir = input()  # the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)

    if 'L' in bomb_dir:
        x1 = x - 1
    elif 'R' in bomb_dir:
        x0 = x + 1

    if 'U' in bomb_dir:
        y1 = y - 1 
    elif 'D' in bomb_dir:
        y0 = y + 1

    x = int(x0 + (x1 - x0) / 2)
    y = int(y0 + (y1 - y0) / 2) 
    
    # the location of the next window Batman should jump to.
    print(str(x) + " " + str(y))