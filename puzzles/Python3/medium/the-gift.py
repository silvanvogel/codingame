import sys
import math

n = int(input())    # number of participants
c = int(input())    # cost of gift
contributions = []
availableBudget = 0

for i in range(n):
    contribution = int(input())
    contributions.append(contribution)
    availableBudget += contribution

if availableBudget < c:
    print("IMPOSSIBLE")
else:
    contributions.sort()

    for i in range(n):
        remaining = math.floor(c / (n - i))
        minContribution = min(contributions[i], remaining)
        
        print(minContribution)
        c-=minContribution