mn=input()
dic={1:".----",2:"..---",3:"...--",4:"....-",5:".....",6:"-....",7:"--...",8:"---..",9:"----.",0:"-----"}
o=""
if len(mn)>10 or not mn.isdigit():print("Untransformable")
else:
    for i in mn: 
        o+=dic[int(i)]+" "
    print(o.rstrip(" "))