n = int(input())
fruits = {}
juices = {}

for i in range(n):
    c, n = input().split()
    fruits[c] = n
    juices[c] = 0
    n = int(n)
line = input()
c=0

for i in line:
    if i in fruits:
        juices[i] += 1
        
for i in juices:
    c += int(juices[i]) // int(fruits[i])

print(c)