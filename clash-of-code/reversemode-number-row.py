n = int(input())
nums = input().split()
res = []
for i in range(0, n, 2):
    if i+1 >= n:
        k = int(nums[i])/2
    else:
        k = (int(nums[i])+ int(nums[i+1]))/2

    # Remove ".0" from integer numbers:
    if k.is_integer():
        res.append(str(int(k)))
    else:
        res.append(str(k))
print(' '.join(res))