i = input()
print("".join(f"{i.count(c)}{c}" for c in sorted(set(i))))