import sys
import math
g=[500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1]
n = float(input())
z=[]
for i in g:
    f=n//i
    if f != 0:
        z+=[f'{int(f)} x {i}']
    n -= i*f
z[-1]+='€'
if n == 0:
    print(*z,sep=', ')
else:
    print('IMPOSSIBLE')