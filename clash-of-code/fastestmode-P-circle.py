x = int(input())
x %= 360
a,b=(1/(180/x)).as_integer_ratio()
if a!=1:
    print(f'{a}*P/{b}')
elif b!=1:
    print(f'P/{b}')
else:
    print('P')
