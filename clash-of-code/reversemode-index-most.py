import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

s = input()
most = 0
c1=0
index=0
start=0
for i in range(len(s)):
    if s[i] == "1":
        c1 += 1
        if c1 == 1:
            start = i+1
    if c1 > most:
        index = start
        most = c1
    if s[i] == "0":
        c1=0

print(index, most)
