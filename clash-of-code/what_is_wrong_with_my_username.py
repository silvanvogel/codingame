username = input()
output = ""
skip = False

for i in range(len(username)):
    if skip:
        skip = False
    else:
        output += username[i]
    
        if username[i].isdigit() and i+1 < len(username):
            if int(username[i]) == 0:
                skip = True
            else: 
                output += username[i+1] * (int(username[i])-1)
                
print(output)
