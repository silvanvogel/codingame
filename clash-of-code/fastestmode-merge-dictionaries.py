d ={}
n1 = int(input())
for i in range(n1):
    k,c = input().split(';')
    d[k] = [c]
n2 = int(input())
for i in range(n2):
    k,c = input().split(";")
    d[k] = d.get(k,[]) + [c]
for k, c in d.items():
    print(f"{k}:",' and '.join(c))