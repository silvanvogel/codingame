import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

v_start, p, d = input().split()
v_start = int(v_start)
p = int(p)
d = float(d)

v = v_start

x = []
for i in range(1000):
    v = int((v+p)*(1-d))
    x.append(v)
    
if p != 0:
    print(x[-1])
else:
    print(0)