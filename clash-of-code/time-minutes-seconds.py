c, s = input().split()
s=s.split(":")
c=c.split(":")
sec=int(s[1])-int(c[1])
m=int(s[0])-int(c[0])
if sec<0:
    m+=-1
    sec+=60
print(str(m)+":"+str(sec).zfill(2))