import sys
import math

crops, tomatoes, delay = [int(i) for i in input().split()]
garden = input()
for i in range(len(garden)):
    if int(garden[i]) <= delay:
        tomatoes -= 1

if tomatoes <= 0:
    print("YOU_CAN_MAKE_A_SOUP_IN_", delay, "_DAYS", sep = "")
else:
    print("YOU_CANNOT_MAKE_A_SOUP_IN_", delay, "_DAYS", sep = "")