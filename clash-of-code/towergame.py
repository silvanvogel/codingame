# build tower with p + q
n=int(input())
p,q=[int(i)for i in input().split()]
allowed=[]
for i in range(1000):
    for j in range(1000):
        if i*p+j*q==n: allowed.append(i+j)
try:
    print(max(allowed))
except:
    print(0)