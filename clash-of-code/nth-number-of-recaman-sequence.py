import sys
import math
n = int(input())
g=[0]
for i in range(1,n+1):
    s=g[i-1]-i
    if s > 0 and s not in g:
        g+=[s]
    else:
        g+=[g[i-1]+i]
print(g[n])