no = ['Dr', 'Mr', 'Mrs', 'Ms' , 'Lord', 'Lady', 'Sir', 'BA', 'LLB' , 'MD', 'PhD', 'Jr', 'Snr']
no = [i.lower() for i in no]
n = int(input())
for j in range(n):
    name = input()
    print(*[i[0].upper() for i in name.split() if i.lower().replace(",","") not in no],sep=".",end=".\n")