def encrypt(text,s):
    result = ""
    for i in range(len(text)):
        char = text[i]
        if (char.isupper()):
            result += chr((ord(char) + s-65) % 26 + 65)
        else:
            result += chr((ord(char) + s - 97) % 26 + 97)
    return result

a = input()
b = input()
if encrypt(b, (ord(a[0]) - ord(b[0]))) == a:
    print("true")
else:
    print("false")
