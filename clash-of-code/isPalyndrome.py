t=''.join([i.lower() for i in input() if i!= "." and i != "," and i != " "])
def isPal(t):
    for i in range(0, int(len(t)/2)): 
        if ord(t[i]) != ord(t[len(t)-i-1]):
            return "NO"
    return "YES"
print(isPal(t))