import sys
import math

cx, cy = [int(i) for i in input().split()]
r = int(input())
n = int(input())
insideCircle = 0
for i in range(n):
    px, py = [int(j) for j in input().split()]
    if (px - cx)**2 + (py - cy)**2 < r**2:
        insideCircle += 1

print(math.floor(insideCircle / n * 100))