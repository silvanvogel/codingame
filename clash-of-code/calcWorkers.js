const n = parseInt(readline());
a=Array(n).fill(0);
const s = parseInt(readline());
var inputs = readline().split(' ');
function minIndex(arr) {
    var min = Math.min(...arr);
    return arr.indexOf(min);
}

for (let i = 0; i < s; i++) {
    const t = parseInt(inputs[i]);
    a[minIndex(a)]+=t;
}

console.log(Math.max(...a));